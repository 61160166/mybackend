const Applicant = require('../models/Applicant')

const applicantsController = {
  applicantList: [],

  async addApplicant  (req, res, next) {
    const payload = req.body
    const applicant = new Applicant(payload)
    try {
      await applicant.save()
      res.json(applicant)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async updateApplicant (req, res, next) {
    const payload = req.body
    try {
      const applicant = await Applicant.updateOne({ _id: payload._id }, payload)
      res.json(applicant)
    } catch (err) {
      console.log(err)
      res.status(500).send(err)
    }
  },
  async deleteApplicant (req, res, next) {
    const { id } = req.params
    try {
      const applicant = await Applicant.deleteOne({ _id: id })
      res.json(applicant)
      console.log(applicant)
    } catch (err) {
      res.status(500).send(err)
      console.log(err)
    }
  },
  async getApplicants (req, res, next) {
    try {
      const applicants = await Applicant.find({})
      res.json(applicants)
    } catch (err) {
      res.status(500).send(err)
    }
  },

  async getApplicant (req, res, next) {
    const { id } = req.params
    try {
      const applicant = await Applicant.findById(id)
      res.json(applicant)
    } catch (err) {
      res.status(500).send(err)
    }
  }
}

module.exports = applicantsController
