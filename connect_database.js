const mongoose = require('mongoose')
const Applicant = require('./models/Applicant')

mongoose.connect('/mongodb://admin:password@localhost/mydb', {
  useNewUrlParser: true,
  useUnifiedTopology: true
})

const db = mongoose.connection

db.on('error', console.error.bind(console, 'connection error: '))
db.once('open', function () {
  console.log('connect')
})

Applicant.find(function(err, applicants) {
  if (err) return console.error(err)
  console.log(applicants)
})
