const express = require('express')
const cors = require('cors')
const path = require('path')
const cookieParser = require('cookie-parser')
const logger = require('morgan')

const indexRouter = require('./routes/index')
const usersRouter = require('./routes/users')
const helloRouter = require('./routes/hello')
const productsRouter = require('./routes/products')
const applicantRouter = require('./routes/applicant')

const app = express()

app.use(logger('dev'))
app.use(express.json())
app.use(express.urlencoded({ extended: false }))
app.use(cookieParser())
app.use(express.static(path.join(__dirname, 'public')))
app.use(cors())

app.use('/', indexRouter)
app.use('/users', usersRouter)
app.use('/hello', helloRouter)
app.use('/products', productsRouter)
app.use('/applicant', applicantRouter)

module.exports = app
