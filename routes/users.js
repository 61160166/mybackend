const express = require('express')
const router = express.Router()
const usersController = require('../controller/UsersController')

const mongoose = require('mongoose')
mongoose.connect('/mongodb://admin:password@localhost/mydb', {
  useNewUrlParser: true,
  useUnifiedTopology: true
})

const db = mongoose.connection

db.on('error', console.error.bind(console, 'connection error: '))
db.once('open', function () {
  // console.log('connect')
})

/* GET users listing. */
router.get('/', usersController.getUsers)

router.get('/:id', usersController.getUser)

router.post('/', usersController.addUser)

router.put('/', usersController.updateUser)

router.delete('/:id', usersController.deleteUser)

module.exports = router
